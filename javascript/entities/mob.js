/**
 * @param {Mob} def
 */
Mob = function(def) {
	this.pos_on_path = 0;
	this.real_pos_on_path = 0.0;
	this.def = def;
	this.hp = this.def.hp;
	this.speed = this.def.speed;
	this.active_effects = [];

	this.graphics = new AnimatedSprite(
		16,
		16,
		Game.sprite_pool.getList(this.def.sprites),
		this.def.spriteInterval,
		true
	);
}

Mob.prototype.addEffect = function(effect) {
	for (var i = 0; i < this.active_effects.length; i++) {
		if (this.active_effects[i].type == effect.type && (effect.type != 'fire')) {
			if (effect.slowdown > this.active_effects[i].slowdown) {
				this.active_effects.splice(i, 1, effect);
			}
			return;
		}
	}
	this.active_effects.push(effect);
}

/**
 * @param {Number} scale
 */
Mob.prototype.draw = function(scale) {
	var pos = this.getPos();
	this.graphics.draw(pos[0], pos[1], scale);

	// draw hp-bar
	var x = ~~((pos[0] - 8) * scale); // center on x-axis
	var y = ~~((pos[1] - 12) * scale);

	var hp = (this.hp > 0) ? this.hp : 0;
	var hp_percent = (100 / this.def.hp) * hp;
	var width = ~~(16 * scale / 100 * hp_percent);
	Game.ctx.fillStyle = "green";
	Game.ctx.fillRect(x, y, width, 3);

	for (var i = 0; i < this.active_effects.length; i++) {
		this.active_effects[i].draw(this);
	};
}

/**
 * @return {Vector2}
 */
Mob.prototype.getPos = function() {
	return Game.path[this.pos_on_path] || [-9999, 0];
}

/**
 * @return {Vector2}
 */
Mob.prototype.getFuturePos = function(steps) {
	var p = this.pos_on_path + ~~(this.speed * steps);
	return Game.path[p] || [0, 0];
}

Mob.prototype.update = function() {

	// reset speed

	this.speed = this.def.speed;

	for (var i = 0; i < this.active_effects.length; i++) {
		if (this.active_effects[i].lifetime > 0) {
			this.active_effects[i].apply(this);
		} else {
			this.active_effects.splice(i, 1);
			i--;
		}
	};

	if (Game.path[this.pos_on_path]) {
		var pos_now = Game.path[this.pos_on_path];
		var pos_new = Game.path[this.pos_on_path + 1] || Game.path[this.pos_on_path];
		this.position = Game.path[this.pos_on_path];
		if (pos_now[0] == pos_new[0] || pos_now[1] == pos_new[1]) {
			this.real_pos_on_path += this.speed;
		} else {
			this.real_pos_on_path += this.speed * 0.71;
		}
		this.pos_on_path = ~~(this.real_pos_on_path);
	}
}
