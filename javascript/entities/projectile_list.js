ProjectileList = function() {
	this.projectiles = [];
}

ProjectileList.prototype.reset = function(projectile) {
	this.projectiles = [];
}

/**
 * @param {Projectile} projectile
 */
ProjectileList.prototype.add = function(projectile) {
	this.projectiles.push(projectile);
}

/**
 * @param {Number} scale
 */
ProjectileList.prototype.draw = function(scale) {
	for (var i = 0; i < this.projectiles.length; i++) {
		this.projectiles[i].draw(scale);
	}
}

ProjectileList.prototype.update = function() {
	for (var i = 0; i < this.projectiles.length; i++) {
		var projectile = this.projectiles[i];
		if (projectile.pos_on_path >= projectile.path.length) {
			this.projectiles.splice(i, 1);
			i--;
			continue;
		} else {
			projectile.update();
		}
	};
}
