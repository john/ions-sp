function WaveGenerator() {
	this.waves = [];
}

WaveGenerator.prototype.generateMob = function(level, difficulty) {
	var stenght = level * difficulty;
	var speed = 1 + (Math.random() * 2);
	stenght /= speed;
	var hp = 8 * stenght;

	var sprite = DEFINITION.SPRITES.MOBS[~~(Math.random() * DEFINITION.SPRITES.MOBS.length)];
	var value = 10 + level;

	return {
		sprites: [
			sprite
		],
		spriteInterval: 8,
		speed: speed,
		hp: hp,
		value: value
	}
}

WaveGenerator.prototype.generateWave = function(level, difficulty) {
	var count = 5 + ~~(Math.random() * 20);
	var interval = 5 + ~~(Math.random() * 20);

	var wave = {
		interval: interval,
		mobs: []
	}

	for (var i = 0; i < count; i++) {
		var mob = this.generateMob(level, difficulty);
		wave.mobs.push(mob);
	}

	return wave;
}

WaveGenerator.prototype.generateWaves = function(count, difficulty) {
	this.waves = [];
	for (var i = 1; i < count + 1; i++) {
		this.waves.push(this.generateWave(i, difficulty));
	}
}
