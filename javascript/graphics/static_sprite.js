/**
 * @param {Number} width
 * @param {Number} height
 * @param {String} sprite
 */
StaticSprite = function(width, height, sprite) {
	this.width = width;
	this.height = height;
	this.sprite = sprite;
}

StaticSprite.prototype.update = function() {

}

/**
 * @param  {Number} x
 * @param  {Number} y
 * @param  {Number} scale
 */
StaticSprite.prototype.draw = function(x, y, scale) {
	var w = ~~(this.width * scale);
	var h = ~~(this.height * scale);
	var x = ~~(x * scale);
	var y = ~~(y * scale);
	Game.ctx.drawImage(this.sprite, x - w / 2, y - h / 2, w, h);
}
