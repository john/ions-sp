/**
 * @param {Number} width
 * @param {Number} height
 * @param {String[]} sprites
 * @param {Number} sprite_interval
 * @param {Boolean} reverse
 */
AnimatedSprite = function(width, height, sprites, sprite_interval, reverse) {
	this.width = width;
	this.height = height;
	this.sprites = sprites;
	this.sprite_interval = sprite_interval;
	this.nextSprite = 0;
	this.reverse = false;
	this.use_reverse = reverse;

	if (reverse) {
		this.draw = this.drawReverse;
	} else {
		this.draw = this.drawNoReverse;
	}
}

AnimatedSprite.prototype.update = function() {
	if (this.use_reverse) {
		var i = ~~(this.nextSprite / this.sprite_interval);
		if (this.reverse) {
			if (i == 0) {
				this.nextSprite--;
			}
			this.nextSprite--;
		} else {
			if (i == this.sprites.length - 1) {
				this.nextSprite++;
			}
			this.nextSprite++;
		}
	} else {
		this.nextSprite++;
	}
}

/**
 * @param  {Number} x
 * @param  {Number} y
 * @param  {Number} scale
 */
AnimatedSprite.prototype.drawReverse = function(x, y, scale) {
	var i = ~~(this.nextSprite / this.sprite_interval);
	var l = this.sprites.length - 1;
	var w = ~~(this.width * scale);
	var h = ~~(this.height * scale);
	var x = ~~(x * scale);
	var y = ~~(y * scale);

	if (i >= l || i < 0) {
		if (i > l || i < 0) {
			this.reverse = !this.reverse;
		}

		if (i > l) {
			i = l;
		}
		if (i < 0) {
			i = 0;
		}
	}

	Game.ctx.drawImage(this.sprites[i], x - w / 2, y - h / 2, w, h);
}

/**
 * @param  {Number} x
 * @param  {Number} y
 * @param  {Number} scale
 */
AnimatedSprite.prototype.drawNoReverse = function(x, y, scale) {
	var i = ~~(this.nextSprite / this.sprite_interval);
	var w = ~~(this.width * scale);
	var h = ~~(this.height * scale);
	var x = ~~(x * scale);
	var y = ~~(y * scale);

	if (i > this.sprites.length - 1) {
			this.nextSprite = 0;
			i = 0;
	}

	Game.ctx.drawImage(this.sprites[i], x - w / 2, y - h / 2, w, h);
}
