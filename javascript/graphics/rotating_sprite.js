/**
 * @param {Number} width
 * @param {Number} height
 * @param {String} sprite
 */
RotatingSprite = function(width, height, sprites) {
	this.width = width;
	this.height = height;
	this.sprites = sprites;
	this.steps = this.sprites.length;
}

RotatingSprite.prototype.update = function() {

}


/**
 * @param  {Number} x
 * @param  {Number} y
 * @param  {Number} scale
 * @param  {Number} deg
 */
RotatingSprite.prototype.draw = function(x, y, scale, deg) {
	var i = ~~(deg / 360 * this.steps);
	var w = ~~(this.width * scale);
	var h = ~~(this.height * scale);
	var x = ~~(x * scale);
	var y = ~~(y * scale);
	Game.ctx.drawImage(this.sprites[i] || this.sprites[0], x - w / 2, y - h / 2, w, h);
}
