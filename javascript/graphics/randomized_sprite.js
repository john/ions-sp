/**
 * @param {Number} width
 * @param {Number} height
 * @param {String} sprite
 */
RandomizedSprite = function(width, height, sprites) {
	this.width = width;
	this.height = height;
	this.sprites = sprites;
}

RandomizedSprite.prototype.update = function() {

}


/**
 * @param  {Number} x
 * @param  {Number} y
 * @param  {Number} scale
 */
RandomizedSprite.prototype.draw = function(x, y, scale) {
	var i = ~~(Math.random() * this.sprites.length);
	var w = ~~(this.width * scale);
	var h = ~~(this.height * scale);
	var x = ~~(x * scale);
	var y = ~~(y * scale);
	Game.ctx.drawImage(this.sprites[i], x - w / 2, y - h / 2, w, h);
}
