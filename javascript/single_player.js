SinglePlayer = function() {
	this.waveGenerator = new WaveGenerator();
	this.reset();
}

SinglePlayer.prototype.reset = function() {
	this.wave = -1;
	this.waveGenerator.generateWaves(200, 1);
}

SinglePlayer.prototype.nextWave = function() {
	this.wave++;
	if (this.waveGenerator.waves[this.wave]) {
		var w = this.waveGenerator.waves[this.wave];
		$('#wave').text("Wave " + this.wave);
		Game.spawn_interval = w.interval;
		for (var i = 0; i < w.mobs.length; i++) {
			Game.spawnMob(w.mobs[i]);
		}
	} else {
		alert('you won!!!!11');
		Game.reset();
	}
}
