IceEffect = function(lifetime, slowdown, delay) {
	this.lifetime = lifetime;
	this.slowdown = slowdown;
	this.delay = delay || 0;
	this.type = 'ice';
}

IceEffect.prototype.apply = function(mob) {
	if (this.delay <= 0) {
		mob.speed = mob.speed / this.slowdown;
		this.lifetime--;
	} else {
		this.delay--;
	}
}

IceEffect.prototype.draw = function(mob) {
	if (this.delay <= 0) {
		Game.effect_layer.add(new SpriteEffect({
			lifetime: 2,
			target: mob,
			graphics: new RandomizedSprite(
				16,
				16,
				Game.sprite_pool.getList([
					"img/ice_0.png",
					"img/ice_1.png",
					"img/ice_2.png",
				])
			)
		}));
	}
}
