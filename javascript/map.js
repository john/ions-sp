Map = function(canvas, sprite_pool, map) {
	this.canvas = canvas;
	this.ctx = this.canvas.getContext("2d");

	this.sprite_pool = sprite_pool;

	this.spawn_point = [0, 0];
	this.end_point = [0, 0];

	this.block_size = 36;
	this.half_block_size = this.block_size / 2;

	this.fill(map)
}

/**
 * @param {String} input
 */
Map.prototype.fill = function(input) {

	this.input = input;
	var rows = input.tiles;

	this.width = rows[0].length;
	this.height = rows.length;

	this.draw_height = this.height * this.block_size;
	this.draw_width = this.width * this.block_size;

	// update map of blocked pixels
	this.updateObstacleMap();

	//draw default blocks
	for (var row = 0; row < this.height; row++) {
		for (var col = 0; col < this.width; col++) {
			if (rows[row][col].is_spawn_point) {
				this.spawn_point = [
					col * this.block_size + this.half_block_size,
					row * this.block_size + this.half_block_size
				];
			}
			if (rows[row][col].is_end_point) {
				this.end_point = [
					col * this.block_size + this.half_block_size,
					row * this.block_size + this.half_block_size
				];
			}
		};
	};
}

Map.prototype.collisionMap = function() {
	var canvas = document.createElement('canvas');
	canvas.width = this.draw_width;
	canvas.height = this.draw_height;

	var ctx = canvas.getContext('2d');
	for (var row = 0; row < this.height; row++) {
		for (var col = 0; col < this.width; col++) {
			if (!this.input.tiles[row][col].walkable) {
				var x = col * this.block_size + this.half_block_size;
				var y = row * this.block_size + this.half_block_size;
				ctx.beginPath();
				ctx.arc(x, y, 35, 0, 2 * Math.PI, false);
				ctx.fillStyle = 'red';
				ctx.fill();
			}
		};
	};

	return ctx.getImageData(0, 0, canvas.width, canvas.height);
}

Map.prototype.updateObstacleMap = function () {
	var background = this.collisionMap();
	this.obstacle_map = new ObstacleMap(this.draw_width, this.draw_height);
	for (var i = 0; i < this.draw_height; i++) {
		for (var j = 0; j < this.draw_width; j++) {
			if (background.data[(i * this.draw_width + j) * 4] != 0) {
				this.obstacle_map.setAt(j, i, true);
			}
		};
	};
}

/**
 * @param {Number} x
 * @param {Number} y
 * @param {Image} sprite
 */
Map.prototype.add = function(x, y, sprite) {
	this.data[y][x] = sprite;
}

Map.prototype.showPath = function(scale) {
	this.ctx.strokeStyle = 'green';
	for (var i = 0; i < this.path.length; i++) {
		var p = this.path[i];
		this.ctx.strokeRect(p[0] * scale, p[1] * scale, 1, 1);
	};
}

Map.prototype.calculatePath = function() {
	var dim = Math.max(this.draw_width, this.draw_height);
	this.path = findPath(this.obstacle_map, dim, dim, this.spawn_point, this.end_point);
	return this.path;
}

/**
 * @param {Number} scale
 */
Map.prototype.draw = function(scale, editor) {
	this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

	var rows = this.input.tiles;

	this.width = rows[0].length;
	this.height = rows.length;

	for (var row = 0; row < this.height; row++) {
		for (var col = 0; col < this.width; col++) {
			var x = col * this.block_size * scale;
			var y = row * this.block_size * scale;
			this.ctx.drawImage(
				this.sprite_pool.get("img/stars.jpg"),
				x, y,
				this.block_size * scale, this.block_size * scale
			);
		}
	}

	for (var row = 0; row < this.height; row++) {
		for (var col = 0; col < this.width; col++) {
			var x = col * this.block_size * scale;
			var y = row * this.block_size * scale;
			if (rows[row][col].sprite) {
				this.ctx.drawImage(
					this.sprite_pool.get(rows[row][col].sprite),
					x, y,
					this.block_size * scale, this.block_size * scale
				);
			}
			if (editor) {
				this.ctx.lineWidth = 1;
				if (rows[row][col].walkable) {
					this.ctx.strokeStyle = 'red'
					this.ctx.strokeRect(x + 40, y + 40, 5, 5);
				}
				if (rows[row][col].buildable) {
					this.ctx.strokeStyle = 'green'
					this.ctx.strokeRect(x + 45, y + 40, 5, 5);
				}
				if (rows[row][col].is_spawn_point) {
					this.ctx.strokeStyle = 'red'
					this.ctx.strokeRect(x + 40, y + 50, 10, 10);
				}
				if (rows[row][col].is_end_point) {
					this.ctx.strokeStyle = 'green'
					this.ctx.strokeRect(x + 45, y + 50, 10, 10);
				}
				this.ctx.strokeStyle = 'blue'
				this.ctx.strokeRect(x, y, ~~(this.block_size * scale), ~~(this.block_size * scale));
			}
		};
	};
}
