DEFINITION = {};

DEFINITION.SPRITES = {}

DEFINITION.SPRITES.MOBS = [
	"img/en01.png",
	"img/en02.png",
	"img/en03.png",
	"img/en04.png",
	"img/en05.png",
	"img/en06.png",
	"img/en07.png",
	"img/en08.png",
	"img/en09.png",
	"img/en10.png",
	"img/en11.png",
	"img/en12.png",
	"img/en13.png",
	"img/en14.png",
	"img/en15.png",
	"img/en16.png",
	"img/en17.png"
]

DEFINITION.TOWER = [
	{
		name: "laser_tower",
		levels: [
			{
				level: 0,
				price: 600,
				displayName: "weak laser tower",
				rotates: true,
				sprites: [
					'img/lasertower_0.png'
				],
				rotation_steps: 45,
				attack: {
					type: 'laser',
					color: "rgba(240, 40, 80, 0.5)",
					damage: 8,
					range: 60,
					interval: 16
				}
			},
			{
				level: 1,
				price: 800,
				displayName: "laser tower",
				rotates: true,
				sprites: [
					'img/lasertower_1.png'
				],
				rotation_steps: 45,
				attack: {
					type: 'laser',
					color: "rgba(40, 240, 80, 0.5)",
					damage: 12,
					range: 70,
					interval: 14
				}
			},
			{
				level: 2,
				price: 1000,
				displayName: "strong laser tower",
				rotates: true,
				sprites: [
					'img/lasertower_2.png'
				],
				rotation_steps: 45,
				attack: {
					type: 'laser',
					color: "rgba(40, 80, 240, 0.5)",
					damage: 22,
					range: 80,
					interval: 12
				}
			}
		]
	},
	{
		name: "gun_tower",
		levels: [
			{
				level: 0,
				price: 500,
				displayName: "weak gun tower",
				rotates: true,
				sprites: [
					"img/guntower_0.png"
				],
				rotation_steps: 45,
				attack: {
					type: 'bullets',
					bullet_speed: 5,
					damage: 2,
					range: 70,
					interval: 10
				}
			},
			{
				level: 1,
				price: 800,
				displayName: "gun tower",
				rotates: true,
				sprites: [
					"img/guntower_1.png"
				],
				rotation_steps: 45,
				attack: {
					type: 'bullets',
					bullet_speed: 5,
					damage: 4,
					range: 75,
					interval: 7
				}
			},
			{
				level: 2,
				price: 1000,
				displayName: "strong gun tower",
				rotates: true,
				sprites: [
					"img/guntower_2.png"
				],
				rotation_steps: 45,
				attack: {
					type: 'bullets',
					bullet_speed: 5,
					damage: 6,
					range: 80,
					interval: 5
				}
			}
		]
	},
	{
		name: "bomb_tower",
		levels: [
			{
				level: 0,
				price: 500,
				displayName: "weak bomb tower",
				rotates: true,
				sprites: [
					"img/guntower_0.png"
				],
				rotation_steps: 45,
				attack: {
					type: 'bomb',
					bullet_speed: 4,
					splash: 20,
					damage: 6,
					range: 60,
					interval: 30
				}
			},
			{
				level: 0,
				price: 500,
				displayName: "bomb tower",
				rotates: true,
				sprites: [
					"img/guntower_0.png"
				],
				rotation_steps: 45,
				attack: {
					type: 'bomb',
					bullet_speed: 4,
					splash: 30,
					damage: 10,
					range: 60,
					interval: 27
				}
			},
			{
				level: 0,
				price: 500,
				displayName: "strong bomb tower",
				rotates: true,
				sprites: [
					"img/guntower_0.png"
				],
				rotation_steps: 45,
				attack: {
					type: 'bomb',
					bullet_speed: 4,
					splash: 40,
					damage: 12,
					range: 60,
					interval: 25
				}
			}
		]
	},
	{
		name: "sniper_tower",
		levels: [
			{
				level: 0,
				price: 10,
				displayName: "weak sniper tower",
				rotates: true,
				rotation_steps: 45,
				sprites: [
					"img/snipertower_0.png"
				],
				attack: {
					type: 'bullets',
					bullet_speed: 6,
					damage: 20,
					range: 500,
					interval: 80
				}
			},
			{
				level: 1,
				price: 1400,
				displayName: "sniper tower",
				rotates: true,
				rotation_steps: 45,
				sprites: [
					"img/snipertower_1.png"
				],
				attack: {
					type: 'bullets',
					bullet_speed: 6,
					damage: 40,
					range: 500,
					interval: 80
				}
			},
			{
				level: 1,
				price: 1400,
				displayName: "sniper tower",
				rotates: true,
				rotation_steps: 45,
				sprites: [
					"img/snipertower_2.png"
				],
				attack: {
					type: 'bullets',
					bullet_speed: 8,
					damage: 40,
					range: 500,
					interval: 40
				}
			}
		]
	},
	{
		name: "fire_tower",
		levels: [
			{
				level: 0,
				price: 1000,
				displayName: "fire wave tower",
				sprites: [
					"img/fire_tower_0.png"
				],
				spriteInterval: 1,
				attack: {
					type: 'fire_wave',
					damage: 3,
					range: 45,
					interval: 30
				}
			},
			{
				level: 1,
				price: 1200,
				displayName: "strong fire wave tower",
				sprites: [
					"img/fire_tower_1.png"
				],
				spriteInterval: 1,
				attack: {
					type: 'fire_wave',
					damage: 6,
					range: 50,
					interval: 30
				}
			}
		]
	},
	{
		name: "poison_tower",
		levels: [
			{
				level: 0,
				price: 1200,
				displayName: "poison wave tower",
				sprites: [
					"img/poison_tower_0.png"
				],
				spriteInterval: 6,
				attack: {
					type: 'poison_wave',
					damage: 4,
					slowdown: 1.5,
					range: 45,
					interval: 70
				}
			},
			{
				level: 0,
				price: 1000,
				displayName: "poison wave tower",
				sprites: [
					"img/poison_tower_1.png"
				],
				spriteInterval: 6,
				attack: {
					type: 'poison_wave',
					damage: 8,
					slowdown: 2,
					range: 50,
					interval: 70
				}
			}
		]
	},
	{
		name: "ice_tower",
		levels: [
			{
				level: 0,
				price: 700,
				displayName: "ice wave tower",
				sprites: [
					"img/ice_tower_0.png"
				],
				spriteInterval: 6,
				attack: {
					type: 'ice_wave',
					range: 50,
					slowdown: 2,
					interval: 40
				}
			},
			{
				level: 1,
				price: 10,
				displayName: "ice wave tower",
				sprites: [
					"img/ice_tower_1.png"
				],
				spriteInterval: 6,
				attack: {
					type: 'ice_wave',
					range: 55,
					slowdown: 3,
					interval: 40
				}
			}

		]
	}
]

DEFINITION.MAP = [
'{"name":"testmap","tiles":[[{"sprite":"img/tiles/tile_03.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true,"is_spawn_point":true},{"sprite":"img/tiles/tile_03.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_03.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_03.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true,"is_end_point":true}],[{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_03.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_03.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true}],[{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true}],[{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true}],[{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_01.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_01.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_01.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true}],[{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true}],[{"sprite":"img/tiles/tile_13.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_20.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_20.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_07.png","buildable":true,"walkable":false}]]}'
,
'{"name":"testmap","tiles":[[{"sprite":"img/tiles/tile_03.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true,"is_spawn_point":true},{"sprite":"img/tiles/tile_03.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true,"is_end_point":false}],[{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_08.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_07.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true}],[{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true}],[{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_08.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_19.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_07.png","buildable":true,"walkable":false}],[{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_01.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_01.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_01.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true,"is_end_point":true}],[{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_02.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_03.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true},{"sprite":"img/tiles/tile_00.png","buildable":false,"walkable":true}],[{"sprite":"img/tiles/tile_13.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_20.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_20.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_20.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_06.png","buildable":true,"walkable":false},{"sprite":"img/tiles/tile_07.png","buildable":true,"walkable":false}]]}'
]

function getMob(type) {
	for (var i = 0; i < DEFINITION.ENEMY.length; i++) {
		if (DEFINITION.ENEMY[i].name == type) {
			return DEFINITION.ENEMY[i];
		}
	};
}
