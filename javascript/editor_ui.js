$(document).ready(function() {
	$('#set_size').on("click", function (event) {
		Editor.setMapSize($('#map_width').val(), $('#map_height').val());
	});

	$('#set_walkable').on("click", function (event) {
		Editor.setWalkable();
	});

	$('#set_builable').on("click", function (event) {
		Editor.setBuilable();
	});

	$('#set_end_point').on("click", function (event) {
		Editor.setEndPoint();
	});

	$('#set_spawn_point').on("click", function (event) {
		Editor.setSpawnPoint();
	});

	$(window).keydown(function(ev) {
		console.log(ev)
	})
 });
