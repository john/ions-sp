if (typeof(Engine) == "undefined") {
    Engine = {};
}

Engine.Game = function (map_layer, drawing_layer, map) {
    if (typeof Game != "undefined") return;
    Game = this;

    this.settings = {
        debug: localStorage.getItem('GAME_DEBUG') || false,
    };

    this.simulation_speed = 1;
    this.spawn_interval = 20;
    this.ui_tick = 30;
    this.scale = 1;

    this.spawn_timer = 0;
    this.delta = 0;
    this.clock = 0;

    this.last_frame_time_ms = false;

    this.selected = false;

    this.map_layer = map_layer;
    this.drawing_layer = drawing_layer;

    this.ctx = this.drawing_layer.getContext("2d");
    this.effect_layer = new EffectLayer(drawing_layer);

    this.spawn_queue = [];
    this.towers = [];
    this.grid = [];
    this.mob_list = new MobList();
    this.projectile_list = new ProjectileList();
    this.single_player = new SinglePlayer();
    this.sprite_pool = new SpritePool(this);

    this.resetPlayer();

    this.loadSprites();

    this.drawing_layer.onclick = function (ev) {
        Game.handleClick(ev.pageX, ev.pageY - this.offsetTop);
    }

    window.onresize = function () {
        Game.setSize(window.innerWidth);
    }

    this.waitForSpritePool = window.setInterval(function () {
        if (Game.sprite_pool.ready()) {
            Game.loadMap(map);
            Game.initMap();
            window.clearInterval(Game.waitForSpritePool);
        }
    }, 100);
}

Engine.Game.prototype.loadSprites = function () {
    // load tower srites
    for (var tower = 0; tower < DEFINITION.TOWER.length; tower++) {
        for (var level = 0; level < DEFINITION.TOWER[tower].levels.length; level++) {
            var l = DEFINITION.TOWER[tower].levels[level];
            if (l.rotation_steps > 0) {
                Game.sprite_pool.registerRotatingSprite(l.sprites[0], l.rotation_steps);
            } else {
                l.sprites.forEach(function (sprite) {
                    Game.sprite_pool.registerSprite(sprite);
                });
            }
        };
    }
    // load mob srites
    for (var enemy = 0; enemy < DEFINITION.SPRITES.MOBS.length; enemy++) {
        var e = DEFINITION.SPRITES.MOBS[enemy];
        Game.sprite_pool.registerSprite(e);
    }

    this.sprite_pool.registerSprite("img/noise.png");
    this.sprite_pool.registerSprite("img/tiles/tile_01.png");
    this.sprite_pool.registerSprite("img/tiles/tile_00.png");
    this.sprite_pool.registerRotatingSprite("img/spawn.png", 90);
    this.sprite_pool.registerRotatingSprite("img/end.png", 90);
}

Engine.Game.prototype.loadMap = function (map) {
    map = JSON.parse(map);
    if (!map) return;
    this.map = new Map(this.map_layer, this.sprite_pool, map);
    this.buildGrid();
    this.calculatePath();
    this.initMap();
    this.reset();
}

Engine.Game.prototype.initMap = function () {
    this.map.draw(this.scale);
    this.addPortals();
    if (!this.initialized) {
        window.requestAnimationFrame(update);
    }
    this.setSize();
    this.initialized = true;
    $('#loader').addClass('hidden');
}

Engine.Game.prototype.resetPlayer = function () {
    this.player_hp = 100;
    this.player_money = 1000;
}

Engine.Game.prototype.buildGrid = function () {
    this.grid = [];
    for (var i = 0; i < this.map.height; i++) {
        this.grid[i] = [];
        for (var j = 0; j < this.map.width; j++) {
            this.grid[i][j] = false;
        }
    }
}

Engine.Game.prototype.reset = function () {
    this.resetPlayer();
    this.ctx.clearRect(0, 0, this.drawing_layer.width, this.drawing_layer.height);
    this.selected = false;
    this.spawn_queue = [];
    this.towers = [];

    this.single_player.reset();
    this.mob_list.reset();
    this.projectile_list.reset();
    this.effect_layer.reset();
    this.buildGrid();
    this.addPortals();
}

Engine.Game.prototype.debugPrint = function (msg) {
    if (this.settings.debug) {
        console.debug(msg);
    }
}

Engine.Game.prototype.addPortals = function () {
    Game.effect_layer.add(new SpriteEffect({
        lifetime: -1,
        target: new DummyTarget(this.map.spawn_point),
        graphics: new AnimatedSprite(
            36,
            36,
            this.sprite_pool.get('img/spawn.png'),
            1,
            false
        )
    }));

    Game.effect_layer.add(new SpriteEffect({
        lifetime: -1,
        target: new DummyTarget(this.map.end_point),
        graphics: new AnimatedSprite(
            36,
            36,
            this.sprite_pool.get('img/end.png'),
            1,
            false
        )
    }));
}

/**
 * @param {Number} x
 * @param {Number} y
 */
Engine.Game.prototype.handleClick = function (x, y) {
    // translate position on canvas to grid position
    x = Math.floor(this.map.width / this.drawing_layer.width * x);
    y = Math.floor(this.map.height / this.drawing_layer.height * y);

    this.debugPrint('click at: x ' + x + ' y ' + y);

    // store click position
    this.selected = [x, y];

    if (this.grid[y][x] == false && this.map.input.tiles[y][x].buildable) { // block is build area and free
        this.debugPrint('opening towerBuyMenu');

        var tower = this.grid[y][x];
        $('.menu.buy').addClass('active');

    } else if (this.grid[y][x] != false && this.map.input.tiles[y][x].buildable) { // block is build area and not free
        this.debugPrint('opening towerUpgradeMenu');
        var tower = this.grid[y][x];
        buildUpgradeMenu(tower);
        $('.menu.upgrade').addClass('active');
    } else {
        $('.menu.upgrade').removeClass('active');
    }
}

/**
 * @param {Number} width
 */
Engine.Game.prototype.setSize = function () {

    width = Math.floor($(this.drawing_layer).width());

    this.debugPrint('setting Game width to ' + width + ' px');
    this.scale = width / (36 * this.map.width);
    this.drawing_layer.width = width;
    this.drawing_layer.height = width * (this.map.height / this.map.width);
    this.map_layer.width = width;
    this.map_layer.height = width * (this.map.height / this.map.width);
    if (this.map) {
        this.map.draw(this.scale);
    }
}

/**
 * @param {Number} speed
 */
Engine.Game.prototype.setSpeed = function (speed) {
    this.debugPrint('setting Game speed to ' + speed);
    this.simulation_speed = speed;
}

Engine.Game.prototype.getWave = function () {
    return this.single_player.wave;
}

Engine.Game.prototype.nextWave = function () {
    return this.single_player.nextWave();
}

Engine.Game.prototype.calculatePath = function () {
    this.debugPrint('calculating path');
    this.path = this.map.calculatePath();
    this.debugPrint('path length ' + this.path.length);
}

/**
 * @param {Number} x
 * @param {Number} y
 * @param {Number} max_dist
 * @return {Mob}
 */
Engine.Game.prototype.getNearestMob = function (x, y, max_dist) {
    return this.mob_list.getNearestMob(x, y, max_dist);
}

/**
 * @param {Number} x
 * @param {Number} y
 * @param {Number} max_dist
 * @return {Mob[]}
 */
Engine.Game.prototype.getMobsInRange = function (x, y, max_dist) {
    return this.mob_list.getMobsInRange(x, y, max_dist);
}

Engine.Game.prototype.benchmark = function () {
    this.towers = [];
    this.buildGrid();
    for (var i = 0; i < this.grid.length; i++) {
        for (var j = 0; j < this.grid[0].length; j++) {
            if (this.map.input.tiles[i][j].buildable) {
                var index = ~~(Math.random() * DEFINITION.TOWER.length);
                var def = DEFINITION.TOWER[index];
                var t = new Tower(toInternalPosition(j), toInternalPosition(i), def);
                this.towers.push(t);
                this.grid[i][j] = t;
            }
        }
    }
}

Engine.Game.prototype.showPath = function () {
    this.map.showPath(this.scale);
}

/**
 * @param {Projectile} projectile
 */
Engine.Game.prototype.spawnProjectile = function (projectile) {
    this.projectile_list.add(projectile);
}

/**
 * @param {String} mob_type
 */
Engine.Game.prototype.spawnMob = function (def) {
    this.spawn_queue.push(new Mob(def));
}

Engine.Game.prototype.getSelectedTower = function () {
    return this.grid[this.selected[1]][this.selected[0]];
}

Engine.Game.prototype.upgradeTower = function () {
    var tower = this.getSelectedTower();
    if (tower.canUpgrade() && tower.upgradeCost() <= this.player_money) {
        tower.upgrade();
        this.player_money -= tower.upgradeCost();
    }
    return tower;
}

/**
 * @param {Tower} tower
 */
Engine.Game.prototype.spawnTower = function (tower) {
    if (this.selected) {
        var x = this.selected[0];
        var y = this.selected[1];
        var price = tower.levels[0].price;
        if (!this.grid[y][x] && this.map.input.tiles[y][x].buildable && this.player_money >= price) {
            var def = tower;
            var x = this.selected[0];
            var y = this.selected[1];
            var t = new Tower(toInternalPosition(x), toInternalPosition(y), def);
            this.towers.push(t);
            this.grid[y][x] = t;
            this.player_money -= price;
        }
    }
}

Engine.Game.prototype.sellTower = function () {
    var x = this.selected[0];
    var y = this.selected[1];
    var tower = this.grid[y][x];
    this.player_money += tower.sellPrice();
    var i = this.towers.indexOf(tower);
    this.towers.splice(i, 1);
    this.grid[y][x] = false;
}

Engine.Game.prototype.update = function (timestamp) {

    var timestep = 1000 / 60;
    this.delta += timestamp - this.last_frame_time_ms || timestamp;
    this.last_frame_time_ms = timestamp;

    if (this.delta > timestep * 30) {
        this.delta = timestep * 30; //limit delta to prevent glitches
    }

    while (this.delta >= timestep) {
        for (var i = 0; i < this.simulation_speed; i++) {
            if (this.spawn_queue.length > 0) {
                if (this.spawn_timer == 0) {
                    this.mob_list.add(this.spawn_queue.shift());
                    this.spawn_timer = this.spawn_interval;
                } else {
                    this.spawn_timer--;
                }
            }

            for (var j = 0; j < this.towers.length; j++) {
                this.towers[j].attack();
            }
            this.projectile_list.update();
            this.mob_list.update();
            this.effect_layer.update();
        }
        this.delta -= timestep;
        this.player_hp -= this.mob_list.getSurvived();
        this.player_money += this.mob_list.getMoney();
    }

    if (this.single_player && this.player_hp < 1) {
        alert('You loose');
        this.reset();
    }

    if (this.ui_tick == 0) {
        this.map.draw(this.scale)
        $('#playerHp').text(this.player_hp);
        $('#playerMoney').text(this.player_money);
        updatePriceTags(this.player_money);
        this.ui_tick = 30;
    } else {
        this.ui_tick--;
    }

    this.ctx.clearRect(0, 0, this.drawing_layer.width, this.drawing_layer.height);
    var scale = this.scale;
    //this.ctx.shadowColor = 'none';
    this.projectile_list.draw(scale);
    this.mob_list.draw(scale);
    this.effect_layer.draw(scale);

    for (var i = 0; i < this.towers.length; i++) {
        var tower = this.towers[i];
        var pos = tower.position;
        if (tower.rotation) {
            tower.graphics.draw(pos[0], pos[1], scale, tower.rotation);
        } else {
            tower.graphics.draw(pos[0], pos[1], scale);
        }
    }

    if (this.selected) {
        var tower = this.grid[this.selected[1]][this.selected[0]];
        var centerX = toInternalPosition(this.selected[0]) * scale;
        var centerY = toInternalPosition(this.selected[1]) * scale;

        if (tower) {
            var radius = tower.def.levels[tower.level].attack.range * scale;
        } else if (this.map.input.tiles[this.selected[1]][this.selected[0]].buildable) {
            var radius = 15 * scale;
        }

        this.ctx.beginPath();
        this.ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
        this.ctx.lineWidth = 2;
        this.ctx.strokeStyle = 'green';
        this.ctx.stroke();
    }
}

update = function (timestamp) {
    Game.update(timestamp);
    if (!window.stop_game) {
        window.requestAnimationFrame(update);
    }
}
