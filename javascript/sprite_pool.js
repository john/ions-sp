/**
 * @param {Game} parent
 */
SpritePool = function(parent) {
	this.parent = parent;
	this.sprites = {};

	//internal counters
	this.to_load = 0;

	this.loaded = 0;
	this.errors = 0;

	// 1x1 pixel
	this.fallback_data = new Image();
	this.fallback_data.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
}

SpritePool.prototype.ready = function() {
	return this.to_load == this.loaded + this.errors;
};

/**
 * @param  {String} url
 * @return {Image}
 */
SpritePool.prototype.has = function(url) {
	if (this.sprites[url]) {
		return true;
	} else {
		return false;
	}
};

/**
 * @param  {String} url
 * @return {String}
 */
SpritePool.prototype.state = function(url) {
	return this.sprites[url].state || null;
};


/**
 * @param  {String} url
 * @return {Image}
 */
SpritePool.prototype.get = function(url) {
	if (this.has(url) && this.state(url) == 'ok') {
		return this.sprites[url].data;
	} else {
		Game.debugPrint('SpritePool: unknown sprite ' + url + ' requested');
		if (!this.has(url)) {
			this.registerSprite(url);
		}
		return this.fallback_data;
	}
};

/**
 * @param {String[]} urls
 */
SpritePool.prototype.getList = function(urls) {
	var l = [];
	for (var i = 0; i < urls.length; i++) {
		l.push(this.get(urls[i]));
	};
	return l;
};

/**
 * @param {String} url
 */
SpritePool.prototype.registerSprite = function(url) {
	if (this.has(url)) return;
	this.sprites[url] = { state: 'loading', data: null };
	this.to_load++;
	var self = this;
	var i = new Image();
	i.onload = function() {
		Game.debugPrint('SpritePool: sprite ' + url + ' loaded');
		self.sprites[url] = { state: 'ok', data: this };;
		self.loaded++;
	}
	i.onerror = function() {
		Game.debugPrint('SpritePool: sprite ' + url + ' could not be loaded');
		self.sprites[url] = { state: 'error', data: null };
		self.errors++;
	}
	i.src = url;
};

/**
 * @param {String} url
 * @param {Number} steps
 */
SpritePool.prototype.registerRotatingSprite = function(url, steps) {
	if (this.has(url)) return;
	this.sprites[url] = { state: 'loading', data: null };
	this.to_load++;
	var self = this;
	var img = new Image();
	img.onload = function() {

		Game.debugPrint('SpritePool: sprite ' + url + ' loaded');

		var w = this.width;
		var h = this.height;
		var wHalf = w / 2;
		var hHalf = h / 2;

		// canvas used for rotation
		var rotation_canvas = document.createElement('canvas');
		rotation_canvas.width = w;
		rotation_canvas.height = h;
		var context = rotation_canvas.getContext('2d');

		// list of rotated sprites
		var arr = [];

		steps = Game.settings.debug ? 1 : steps;

		// create rotated versions for each step and append to list
		for (var i = 0; i < steps; i++) {
			context.clearRect(0, 0, rotation_canvas.width, rotation_canvas.height);
			context.save();
			context.translate(wHalf, hHalf);
			context.rotate(2 / steps * i * Math.PI);
			context.drawImage(this, -wHalf, -hHalf);
			context.restore();
			var r = document.createElement("img");
			r.src = rotation_canvas.toDataURL("image/png");
			arr.push(r);
		};

		// add sprites to SpritePool
		self.sprites[url] = { state: 'ok', data: arr };
		self.loaded++;
	};

	img.onerror = function() {
		Game.debugPrint('SpritePool: sprite ' + url + ' could not be loaded');
		self.sprites[url] = { state: 'error', data: null };
		self.errors++;
	};

	img.src = url;
};
