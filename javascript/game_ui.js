$(document).ready(function() {
	$('.next-wave').on("click", function (event) {
		Game.nextWave();
	});

	$('.pause').on("click", function (event) {
		Game.setSpeed(0);
	});

	$('.play').on("click", function (event) {
		Game.setSpeed(1);
	});

	$('.fast').on("click", function (event) {
		Game.setSpeed(2);
	});

	$('.faster').on("click", function (event) {
		Game.setSpeed(3);
	});

	$('.menu.upgrade .close').on("click", function (event) {
		$('.menu.upgrade').removeClass('active');
	});

	$('.menu.upgrade .sell').on("click", function (event) {
		Game.sellTower();
		$('.menu.upgrade').removeClass('active');
	});

	$('#giveMoney').on("click", function (event) {
		Game.player_money = 1000000000;
	});

	$('#giveLives').on("click", function (event) {
		Game.player_hp = 1000000000;
	});

	$('#benchmark').on("click", function (event) {
		Game.benchmark();
	});

	$('#showPath').on("click", function (event) {
		Game.showPath();
	});

	$('#cheats, #closeCheats').on("click", function (event) {
		$('#cheat_menu').toggleClass('hidden');
	});

	$('#options, #closeOptions').on("click", function (event) {
		$('#option_menu').toggleClass('hidden');
	});

	$('#maps, #closeMaps').on("click", function (event) {
		$('#map_menu').toggleClass('hidden');
	});

	buildBuyMenu();
	buildMapMenu();
});

function updatePriceTags(player_money) {
	$('.price').each(function(i, elm) {
		if ($(elm).data('price') > player_money) {
			$(elm).addClass('red');
		} else {
			$(elm).removeClass('red');
		}
	});
}

function buildBuyMenu() {
	var ul = document.createElement('ul');
	$(ul).append('<li class="close" onclick="$(\'#tower_buy_menu\').removeClass(\'active\');">close</li>');
	for (var i = 0; i < DEFINITION.TOWER.length; i++) {
		var def = DEFINITION.TOWER[i];
		var level = DEFINITION.TOWER[i].levels[0];
		var li = document.createElement('li');
		var img = towerImage(def, 0);
		li.def = def;

		li.appendChild(img);

		var container = towerDef(def, 0);

		li.appendChild(container);


		$(li).on("click", function(event) {
			Game.spawnTower(this.def);
			$('#tower_buy_menu').removeClass('active');
		});
		ul.appendChild(li);
		document.getElementById('tower_buy_menu').appendChild(ul);
	};
}

function buildMapMenu() {
	var mapMenu = document.getElementById('map_menu')
	for (var i = 0; i < DEFINITION.MAP.length; i++) {
		var button = document.createElement('button');
		button.innerHTML = 'Load Map ' + i;
		button.mapIndex = i;
		$(button).on('click', function(ev) {
			Game.loadMap(DEFINITION.MAP[this.mapIndex]);
		});
		mapMenu.appendChild(button);
	};
	var close = document.createElement('button');
	close.innerHTML = 'close';
	close.id = 'closeMaps';
	mapMenu.appendChild(close);
}

function buildUpgradeMenu(tower) {
	var ul = document.createElement('ul');
	var stats_container = document.createElement('div');

	stats_container.appendChild(towerImage(tower.def, tower.level));
	stats_container.appendChild(towerDef(tower.def, tower.level));

	var button_container = document.createElement('div');

	var button_container_ul = document.createElement('ul');

	var sell = document.createElement('li');
	sell.innerHTML = 'Sell for ' + tower.sellPrice() + ' $';
	$(sell).on("click", function(event) {
		Game.sellTower();
		$('.menu.upgrade').removeClass('active');
	});
	button_container_ul.appendChild(sell);

	if (tower.canUpgrade()) {
		var upgrade = document.createElement('li');
		upgrade.innerHTML = 'Upgrade for ' + tower.upgradeCost() + ' $';
		upgrade.setAttribute('data-price', tower.upgradeCost());
		upgrade.setAttribute('class', 'price');
		upgrade.tower = tower;
		$(upgrade).on("click", function(event) {
			var tower = Game.upgradeTower();
			buildUpgradeMenu(tower);
		});
		button_container_ul.appendChild(upgrade);
	}

	$(button_container_ul).append('<li class="close" onclick="$(\'#tower_menu\').removeClass(\'active\');">close</li>');

	ul.appendChild(stats_container);
	button_container.appendChild(button_container_ul)
	ul.appendChild(button_container);
	document.getElementById('tower_menu').innerHTML = '';
	document.getElementById('tower_menu').appendChild(ul);
}

function towerImage(tower_def, level) {
	if (!tower_def.levels[level]) {
		return document.createElement('img');
	}

	var level = tower_def.levels[level];

	var img = document.createElement('img');
	img.src = level.sprites[0];
	return img;
}

function towerDef(tower_def, level) {

	if (!tower_def.levels[level]) {
		return document.createElement('p');
	}

	var level = tower_def.levels[level];
	var container = document.createElement('p');

	var name = document.createElement('label');
	name.innerHTML = level.displayName;
	container.appendChild(name);

	var price = document.createElement('label');
	price.innerHTML = 'Price: ' + level.price + ' $';
	price.setAttribute('data-price', level.price);
	price.setAttribute('class', 'price');
	container.appendChild(price);


	var range = document.createElement('label');
	range.innerHTML = 'Range: ' + level.attack.range;
	container.appendChild(range);

	var interval = document.createElement('label');
	interval.innerHTML = 'Interval: ' + level.attack.interval;
	container.appendChild(interval);

	if (level.attack.damage) {
		var damage = document.createElement('label');
		damage.innerHTML = 'Damage: ' + level.attack.damage;
		container.appendChild(damage);
	}

	if (level.attack.slowdown) {
		var slowdown = document.createElement('label');
		slowdown.innerHTML = 'Slowdown: ' + level.attack.slowdown;
		container.appendChild(slowdown);
	}

	return container;
}
