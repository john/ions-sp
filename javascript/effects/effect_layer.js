EffectLayer = function() {
	this.effects = [];
}

EffectLayer.prototype.reset = function() {
	this.effects = [];
}

/**
 * @param {Effect} effect
 */
EffectLayer.prototype.add = function(effect) {
	this.effects.push(effect);
}

EffectLayer.prototype.update = function() {
	for (var i = 0; i < this.effects.length; i++) {
		var e = this.effects[i];
		if (e.lifetime != 0) {
			e.update();
			if (e.lifetime > 0) {
				e.lifetime--;
			}
		} else {
			this.effects.splice(i, 1);
			i--;
		}
	};
}

/**
 * @param {Number} scale
 */
EffectLayer.prototype.draw = function(scale) {
	for (var i = 0; i < this.effects.length; i++) {
		this.effects[i].draw(scale, Game.ctx);
	}
}
