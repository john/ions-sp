SpriteEffect = function(def) {
	this.def = def;
	this.lifetime = def.lifetime;
	this.delay = def.delay || 0;
}

SpriteEffect.prototype.update = function(scale) {
	this.def.graphics.update();
	this.delay--;
}


SpriteEffect.prototype.draw = function(scale) {
	if (this.delay <= 0) {
		var position = this.def.target.getPos();
		this.def.graphics.draw(position[0], position[1], scale);
	}
}
