if (typeof(Engine) == "undefined") {
	Engine = {};
}

Engine.Editor = function (drawing_layer) {
	if (typeof Editor != "undefined") return;
	Editor = this;

	this.scale = 1;

	this.selected = false;

	this.drawing_layer = drawing_layer;

	this.ctx = this.drawing_layer.getContext("2d");

	this.sprite_pool = new SpritePool(this);

	this.loadSprites();

	this.drawing_layer.onclick = function (ev) {
		Editor.handleClick(ev.pageX, ev.pageY - this.offsetTop);
	}

	window.onresize = function () {
		Editor.setSize(window.innerWidth);
	}

	map = {
		name: 'testmap',
		tiles: [[]]
	}

	for (var row = 0; row < 9; row++) {
		map.tiles[row] = []
		for (var col = 0; col < 15; col++) {
			map.tiles[row][col] = {
				sprite: 'img/tiles/tile_00.png',
				buildable: false,
				walkable: true
			}
		};
	};

	this.waitForSpritePool = window.setInterval(function () {
		if (Game.sprite_pool.ready()) {
			window.clearInterval(Game.waitForSpritePool);
			Game.loadMap(map);
			Game.setSize();
			window.requestAnimationFrame(update);
		}
	}, 100);
}

Engine.Editor.prototype.setMapSize = function (w, h) {

	this.stop = true;

	w = parseInt(w);
	h = parseInt(h);

	if (w < 1) w = 1;
	if (w > 100) w = 100;
	if (h < 1) h = 1;
	if (h > 100) h = 100;

	if (this.map.input.tiles.length < h) {
		while (this.map.input.tiles.length < h) {
			var new_row = [];
			for (var i = 0; i < w; i++) {
				new_row.push({
					sprite: 'img/tiles/tile_00.png',
					buildable: false,
					walkable: true
				});
			};
			this.map.input.tiles.push(new_row);
		}
	} else if (this.map.input.tiles.length > h) {
		while (this.map.input.tiles.length > h) {
			this.map.input.tiles.pop();
		}
	}

	if (this.map.input.tiles[0].length < w) {
		for (var i = 0; i < h; i++) {
			while (this.map.input.tiles[i].length < w) {
				this.map.input.tiles[i].push({
					sprite: 'img/tiles/tile_00.png',
					buildable: false,
					walkable: true
				});
			}
		};
	} else if (this.map.input.tiles[0].length > w) {
		for (var i = 0; i < h; i++) {
			while (this.map.input.tiles[i].length > w) {
				this.map.input.tiles[i].pop();
			}
		}
	}

	this.map.fill(this.map.input);

	this.stop = false;

	this.setSize();
}


Engine.Editor.prototype.setImg = function (url) {
	this.map.input.tiles[this.selected[1]][this.selected[0]].sprite = url;
}

Engine.Editor.prototype.setWalkable = function () {
	this.map.input.tiles[this.selected[1]][this.selected[0]].walkable = !this.map.input.tiles[this.selected[1]][this.selected[0]].walkable;
	this.map.input.tiles[this.selected[1]][this.selected[0]].buildable = !this.map.input.tiles[this.selected[1]][this.selected[0]].walkable;
}

Engine.Editor.prototype.setBuilable = function () {
	this.map.input.tiles[this.selected[1]][this.selected[0]].buildable = !this.map.input.tiles[this.selected[1]][this.selected[0]].buildable;
	this.map.input.tiles[this.selected[1]][this.selected[0]].walkable = !this.map.input.tiles[this.selected[1]][this.selected[0]].buildable;
}

Engine.Editor.prototype.setSpawnPoint = function () {
	this.map.input.tiles[this.selected[1]][this.selected[0]].is_spawn_point = !this.map.input.tiles[this.selected[1]][this.selected[0]].is_spawn_point
	this.map.input.tiles[this.selected[1]][this.selected[0]].buildable = false;
	this.map.input.tiles[this.selected[1]][this.selected[0]].walkable = true;
	this.map.fill(this.map.input);
}

Engine.Editor.prototype.setEndPoint = function () {
	this.map.input.tiles[this.selected[1]][this.selected[0]].is_end_point = !this.map.input.tiles[this.selected[1]][this.selected[0]].is_end_point
	this.map.input.tiles[this.selected[1]][this.selected[0]].buildable = false;
	this.map.input.tiles[this.selected[1]][this.selected[0]].walkable = true;
	this.map.fill(this.map.input);
}

Engine.Editor.prototype.loadSprites = function () {
	this.sprite_pool.registerSprite("img/noise.png");
	for (var i = 0; i < 21; i++) {
		if (i < 10) {
			this.sprite_pool.registerSprite("img/tiles/tile_0" + i + ".png");
		} else {
			this.sprite_pool.registerSprite("img/tiles/tile_" + i + ".png");
		}
	};
}

Engine.Editor.prototype.loadMap = function (map) {
	this.map = new Map(this.drawing_layer, this.sprite_pool, map);
	Game.setSize();
	this.calculatePath();
}

Engine.Editor.prototype.debugPrint = function (msg) {
	console.debug(msg);
}

/**
 * @param {Number} x
 * @param {Number} y
 */
Engine.Editor.prototype.handleClick = function (x, y) {
	// translate position on canvas to grid position
	x = Math.floor(this.map.width / this.drawing_layer.width * x);
	y = Math.floor(this.map.height / this.drawing_layer.height * y);

	this.debugPrint('click at: x ' + x + ' y ' + y);

	// store click position
	this.selected = [x, y];
}

Engine.Editor.prototype.setSize = function () {

	width = Math.floor($(this.drawing_layer).width());

	this.debugPrint('setting Editor width to ' + width + ' px');
	this.scale = width / (36 * this.map.width);
	this.drawing_layer.width = width;
	this.drawing_layer.height = width * (this.map.height / this.map.width);
	if (this.map) {
		this.map.draw(this.scale);
	}
}

Engine.Editor.prototype.calculatePath = function () {
	this.debugPrint('calculating path');
	this.path = findPath(this.map.obstacle_map, 720, 720, this.map.spawn_point, this.map.end_point);
	this.debugPrint('path length ' + this.path.length);
}

Engine.Editor.prototype.showPath = function () {
	this.calculatePath();
	var ctx = this.map_layer.getContext("2d");
	ctx.strokeStyle = 'green';
	for (var i = 0; i < this.path.length; i++) {
		var p = this.path[i];
		ctx.strokeRect(p[0] * this.scale, p[1] * this.scale, 1, 1);
	};
}

Engine.Editor.prototype.update = function (timestamp) {

	if (!this.stop) {
		var scale = this.scale;

		this.map.draw(scale, true);

		if (this.selected) {
			var centerX = toInternalPosition(this.selected[0]) * scale;
			var centerY = toInternalPosition(this.selected[1]) * scale;

			var radius = 15 * scale;

			this.ctx.beginPath();
			this.ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
			this.ctx.lineWidth = 2;
			this.ctx.strokeStyle = 'green';
			this.ctx.stroke();
		}
	}
}

update = function (timestamp) {
	Editor.update(timestamp);
	window.requestAnimationFrame(update);
}
